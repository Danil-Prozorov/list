<?php include 'DB_Request.php';

class RenderList
{
    protected $db_request;

    public function __construct()
    {
        $this->db_request = new DB_Request();
    }

    public function renderOutput($p = 0)
    {
        $parents    = $this->getAllParents($p);
        $result_arr = "";

        foreach ($parents as $row) {
            if($this->hasChild($row['id'])) {
                $childs_arr = $this->getChilds($row['id']);
                $child_resp = "";
                foreach ($childs_arr as $child) {
                    $childs  = '';
                    $childs .= $this->renderOutput(+$child['id']);
                    $child_resp .= "<ul class='child-directory'>".$child['title']."<span>$childs</span>"."</ul>";
                }
                $result_arr .= "<ul id='item_".$row['id']."' class='parent' onclick='return changeVisibl(".+$row['id'].",`off`)'>".$row['title']."<span>$child_resp</span></ul>";
            } else {
                $result_arr .= "<ul id='item_".$row['id']."'>".$row['title']."</ul>";
            }

        }
        return $result_arr;
    }

    protected function getAllParents($id)
    {
        $q       = $this->db_request;
        $request = "SELECT * FROM `lists` WHERE `parent` = $id";
        $query   = $q->query($request);
        $arr     = [];
        while($row = mysqli_fetch_assoc($query)) {
            array_push($arr, $row);
        }

        return $arr;
    }

    protected function getChilds($parent)
    {
        $q       = $this->db_request;
        $request = "SELECT * FROM `lists` WHERE `parent` = $parent";
        $query   = $q->query($request);
        $arr     = [];
        while($row = mysqli_fetch_assoc($query)) {
            array_push($arr, $row);
        }

        return $arr;
    }

    protected function hasChild($item_id)
    {
        $q       = $this->db_request;
        $request = "SELECT * FROM `lists` WHERE `parent` = $item_id";
        $query   = $q->query($request);
        $arr     = [];
        while($row = mysqli_fetch_assoc($query)) {
            array_push($arr, $row);
        }

        if(!empty($arr)){
            return true;
        }

        return false;
    }

}