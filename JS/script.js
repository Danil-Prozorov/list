function changeVisibl(id, status){
    if(status == 'on'){
        let details = document.getElementById(`item_${id}`);
        let child   = details.childNodes;
            child[1].style.display = 'block';
            details.removeAttribute('onclick');
            details.setAttribute('onclick', `return changeVisibl(${id},'off')`);


    }else if(status == 'off'){
        let details = document.getElementById(`item_${id}`);
        let child   = details.childNodes;
            child[1].style.display = 'none';
            details.removeAttribute('onclick');
            details.setAttribute('onclick', `return changeVisibl(${id},'on')`);

    }
}