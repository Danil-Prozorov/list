<?php include 'Services/RenderList.php'; ?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="CSS/style.css">
    <title>List</title>
</head>
<body>
    <?php
        $render = new RenderList();
        echo $render->renderOutput();
    ?>
<script src="JS/script.js"></script>
</body>
</html>